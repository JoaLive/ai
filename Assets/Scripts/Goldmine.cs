﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goldmine : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other) {
		
		if (other.gameObject.tag == "Worker")
		{
			other.gameObject.GetComponent<WorkerController>().StartGathering(transform);
		}
	}
}
