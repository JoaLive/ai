﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour {

    private WorkerController worker;
	void Start () {
        worker = GameObject.Find("Worker").GetComponent<WorkerController>();
	}

    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit = new RaycastHit();
        if (Input.GetMouseButtonUp(0))
        {
            
            if (Physics.Raycast(ray, out hit, 500))
            {
                if (hit.transform.gameObject.tag == "Terrain")
                {
                    worker.GoTo(hit.point);
                }
                else if (hit.transform.gameObject.tag == "Goldmine")
                {
                    worker.GoTo(hit.point);
                }
            }
        }
    }
}
