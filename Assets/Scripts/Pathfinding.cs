﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour {

	public float speed = 30f;
	public float minDist = 3f;

	private PFNode destNode;
	private PFNode originNode;
	private List<PFNode> nodes;
	private List<PFNode> openNodes;
	private List<PFNode> closedNodes;
	private List<PFNode> pathNodes;
	private bool moving = false;
	private int currentNodeIndex = -1;
	void Start () {
		nodes = new List<PFNode>(GameObject.FindGameObjectWithTag("PFNodes").GetComponentsInChildren<PFNode>());
		openNodes = new List<PFNode>();
		closedNodes = new List<PFNode>();
		pathNodes = new List<PFNode>();
	}
	
	void Update () {
		if (!moving)
			return;
		
		Vector3 destPos = pathNodes[currentNodeIndex].transform.position;
		transform.position = Vector3.Lerp(transform.position, destPos, speed * Time.deltaTime);
		if (Vector3.Distance(transform.position, destPos) < minDist)
		{
			if (pathNodes[currentNodeIndex] == destNode)
			{
				moving = false;
				pathNodes.Clear();
			}
			else
				currentNodeIndex++;
		}			
	}

	public void MoveTo(Vector3 dest)
	{		
		SetOriginNode();
		SetDestNode(dest);
		CreatePath();
		moving = true;
		currentNodeIndex = 0;
	}

	private void CreatePath()
	{
		OpenNode(originNode);

		while (openNodes.Count > 0)
		{
			//PFNode node = GetBreadFirstNode ();
			//PFNode node = GetDepthFirstNode ();
			//PFNode node = GetDijkstraNode ();
            PFNode node = GetAStarNode();

            if (node == destNode)
			{
				CreateFinalPath();
				openNodes.Clear();
				closedNodes.Clear();
			}

			else
			{
				openNodes.Remove(node);
				closedNodes.Add(node);

				OpenNode(node);
			}
		}
	}

	private void SetDestNode(Vector3 dest)
	{
		float minDist = Vector3.Distance(nodes[0].transform.position, dest);
		PFNode finalNode = nodes[0];

		foreach (PFNode node in nodes)
		{
			float dist = Vector3.Distance(node.transform.position, dest);
			if (dist < minDist)
			{
				minDist = dist;
				finalNode = node;
			}
		}

		destNode = finalNode;
	}

	private void SetOriginNode()
	{
		float minDist = Vector3.Distance(nodes[0].transform.position, transform.position);
		PFNode finalNode = nodes[0];

		foreach (PFNode node in nodes)
		{
			float dist = Vector3.Distance(node.transform.position,  transform.position);
			if (dist < minDist)
			{
				minDist = dist;
				finalNode = node;
			}
		}

		originNode = finalNode;
		originNode.Cost = 0;
	}

	private void OpenNode(PFNode origin)
	{
		foreach (PFNode node in origin.conectedNodes)
		{
			if (!openNodes.Contains(node) && !closedNodes.Contains(node))
			{
				node.Cost = Vector3.Distance(node.transform.position,  origin.transform.position) + origin.Cost;
                node.ACost = Vector3.Distance(node.transform.position, destNode.transform.position) + node.Cost;
                node.Parent = origin;
				openNodes.Add(node);
			}
		}
	}

	private PFNode GetBreadFirstNode()
	{
		return openNodes[openNodes.Count-1];
	}

	private PFNode GetDepthFirstNode()
	{
		return openNodes[0];
	}

	private PFNode GetDijkstraNode()
	{
		float minCost = openNodes[0].Cost;
		int minCostIndex = 0;
		for (int i = 0; i < openNodes.Count; i++)
		{
			if (openNodes[0].Cost < minCost)
			{
				minCost = openNodes[0].Cost;
				minCostIndex = i;
			}
		}

		return openNodes[minCostIndex];
	}

    private PFNode GetAStarNode() {
        float minACost = openNodes[0].ACost;
        int minCostIndex = 0;
        for(int i = 0; i < openNodes.Count; i++) {
            if(openNodes[0].ACost < minACost) {
                minACost = openNodes[0].ACost;
                minCostIndex = i;
            }
        }

        return openNodes[minCostIndex];
    }

    private void CreateFinalPath()
	{
		PFNode node = destNode;
		while (node != originNode)
		{
			pathNodes.Insert(0, node);
			node = node.Parent;
		}
	}

	public bool IsFinished()
	{
		if (pathNodes[currentNodeIndex] == destNode)
			return true;
		
		return false;
	}

    private void OnDrawGizmos()
    {
        foreach(PFNode node in nodes)
        {
            foreach(PFNode childNode in node.conectedNodes)
            {
                Gizmos.DrawLine(node.transform.position, childNode.transform.position);
            }
        }
    }
}
