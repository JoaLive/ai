﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.AI;

public enum States
{
    idle,
    walking,
    gathering
}

public enum Events
{
    move,
    gather,
    stop
}

public class WorkerController : MonoBehaviour {

    private FSM fsm;
    private Vector3 destPos;
    private NavMeshAgent navMesh;
    private Pathfinding pathfinding;
    private Transform basePosition;
    private Transform prevGoldmine;

    private float gatherTimer = 0;
    private float gold = 0;
    

    public float GatherDuration = 3;
    public float MaxGold = 100;


    void Start ()
    {
        fsm = new FSM(Enum.GetNames(typeof(States)).Length, Enum.GetNames(typeof(Events)).Length);

        fsm.SetRelation((int)States.idle, (int)Events.move, (int)States.walking);
        fsm.SetRelation((int)States.idle, (int)Events.gather, (int)States.gathering);
        fsm.SetRelation((int)States.idle, (int)Events.stop, (int)States.idle);

        fsm.SetRelation((int)States.walking, (int)Events.move, (int)States.walking);
        fsm.SetRelation((int)States.walking, (int)Events.gather, (int)States.gathering);
        fsm.SetRelation((int)States.walking, (int)Events.stop, (int)States.idle);

        fsm.SetRelation((int)States.gathering, (int)Events.move, (int)States.walking);
        fsm.SetRelation((int)States.gathering, (int)Events.gather, (int)States.gathering);
        fsm.SetRelation((int)States.gathering, (int)Events.stop, (int)States.idle);

        destPos = Vector3.zero;

        navMesh = GetComponent<NavMeshAgent>();
        pathfinding = GetComponent<Pathfinding>();
        basePosition = GameObject.FindGameObjectWithTag("Base").transform;
    }
	
	void Update ()
    {
        switch (fsm.GetState())
        {
            case (int)States.walking:
                pathfinding.MoveTo(destPos);
                if (pathfinding.IsFinished())
                    fsm.SetEvent((int)Events.stop);
                break;

            case (int)States.gathering:
                Gather();
                break;

            case (int)States.idle:
                break;
            default:
                break;
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            prevGoldmine = null;
            fsm.SetEvent((int)Events.stop);
        }

    }

    public void GoTo(Vector3 destPos)
    {
        prevGoldmine = null;
        fsm.SetEvent((int)Events.move);
        this.destPos = new Vector3(destPos.x, transform.position.y, destPos.z);
    }

    public void StartGathering(Transform goldmineTransform)
    {
        if(gold < MaxGold)
        {
            fsm.SetEvent((int)Events.gather);
            prevGoldmine = goldmineTransform;
        }
    }

    public void ReturnToGoldmine()
    {
        fsm.SetEvent((int)Events.move);
        Vector3 goldPos = prevGoldmine.position;
        this.destPos = new Vector3(goldPos.x, transform.position.y, goldPos.z);
    }

    public void Gather()
    {
        gatherTimer += Time.deltaTime;

        if (gatherTimer >= GatherDuration)
        {
            gold += 25;
            gatherTimer -= GatherDuration;
        }

        if (gold >= MaxGold)
        {
            ReturnToBase();
        }
    }

    public void ReturnToBase()
    {
        fsm.SetEvent((int)Events.move);
        Vector3 basePos = basePosition.position;
        this.destPos = new Vector3(basePos.x, transform.position.y, basePos.z);
    }

    public void InBase()
    {     
        navMesh.Stop();
        gold = 0;
        if (prevGoldmine)
        {
            ReturnToGoldmine();
        }
    }

    public float GetGold()
    {
        return gold;
    }

    public string GetState()
    {
        switch (fsm.GetState())
        {
            case (int)States.walking:
                return "Walking";
                break;

            case (int)States.gathering:
                return "Gathering";
                break;

            case (int)States.idle:
                return "Idle";
                break;
        }
        return null;
    }
}
