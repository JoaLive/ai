﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM {

	private int [,] fsm;
	private int state;

	public FSM (int statesCount, int eventsCount)
	{
		fsm = new int[statesCount, eventsCount];
        for (int i = 0; i < fsm.GetLength(0); i++)
        {
            for (int j = 0; j < fsm.GetLength(1); j++)
            {
                fsm[i, j] = -1;
            }
        }
	}

	public void SetRelation(int srcState, int srcEvent, int destState)
	{
        fsm[srcState, srcEvent] = destState;
	}

    public int GetState()
    {
        return state;
    }

    public void SetEvent(int srcEvent)
    {
        if (fsm[state, srcEvent] != -1)
            state = fsm[state, srcEvent];
    }
}
