﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PFNode : MonoBehaviour {

	public float Cost { get; set; }
    public float ACost { get; set; }
	public PFNode Parent { get; set; }
	public List<PFNode> conectedNodes;

}
