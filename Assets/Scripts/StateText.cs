﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StateText : MonoBehaviour {

	private Text text;
	private WorkerController worker;
	// Use this for initialization
	void Start () {
		worker = GameObject.Find("Worker").GetComponent<WorkerController>();
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		text.text = "State: " + worker.GetState();
	}
}
